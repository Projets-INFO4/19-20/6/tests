import pycom
import time

from network import Bluetooth
import ubinascii
bluetooth = Bluetooth()

# On commence un scan pendant 10s, après le bluetooth s'éteint si on n'a pas réussi à se connecter à quelqu'un

bluetooth.start_scan(10)

# On désactive le clignotement
pycom.heartbeat(False)
i = 0
while True:
    pycom.rgbled(0x111100)  # Magenta
    time.sleep(1)
    pycom.rgbled(0x001111)  # Cyan
    time.sleep(1)
    pycom.rgbled(0x110011)  # Jaune
    time.sleep(1)
